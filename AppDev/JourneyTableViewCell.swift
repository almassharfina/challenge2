//
//  JourneyTableViewCell.swift
//  AppDev
//
//  Created by Almas Sharfina on 13/04/20.
//  Copyright © 2020 Almas Sharfina. All rights reserved.
//

import UIKit

class JourneyTableViewCell: UITableViewCell {

    @IBOutlet weak var TypeChallengeLabel: UILabel!
    @IBOutlet weak var ChallengeLabel: UILabel!
    @IBOutlet weak var JourneyImageView: UIImageView!
    
       
}
