//
//  ExplorersJuniorCollectionViewCell.swift
//  AppDev
//
//  Created by Almas Sharfina on 16/04/20.
//  Copyright © 2020 Almas Sharfina. All rights reserved.
//

import UIKit

class ExplorersJuniorCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var ExpJuniorImage: UIImageView!
    @IBOutlet weak var ExpJuniorName: UILabel!
    @IBOutlet weak var ExpJuniorRole: UILabel!
}
