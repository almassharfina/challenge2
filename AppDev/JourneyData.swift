//
//  JourneyData.swift
//  AppDev
//
//  Created by Almas Sharfina on 13/04/20.
//  Copyright © 2020 Almas Sharfina. All rights reserved.
//

import Foundation
import UIKit

struct Journey {
    var ImageJourney : String
    var ChallengeJourney : String
    var TypeJourney : String
    
}
class JourneyData {
    var ArrayJourney : [Journey] = [
        Journey(ImageJourney: "challenge2", ChallengeJourney: "Challenge 2", TypeJourney: "Individual Challenge" ),
        Journey(ImageJourney: "challenge3", ChallengeJourney: "Challenge 3", TypeJourney: "Group Challenge" ),
        Journey(ImageJourney: "challenge4", ChallengeJourney: "Challenge 4", TypeJourney: "Group Challenge" ),
        Journey(ImageJourney: "challenge5", ChallengeJourney: "Challenge 5", TypeJourney: "Individual Challenge"),
        Journey(ImageJourney: "challenge6", ChallengeJourney: "Challenge 6", TypeJourney: "Group Challenge"),
        Journey(ImageJourney: "challenge1", ChallengeJourney: "Challenge 1", TypeJourney: "Group Challenge")
        
    ]
}
