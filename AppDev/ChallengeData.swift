//
//  ChallengeData.swift
//  AppDev
//
//  Created by Almas Sharfina on 13/04/20.
//  Copyright © 2020 Almas Sharfina. All rights reserved.
//

import Foundation
import UIKit

struct Challenge {
    var ChallengeStatus : String
    var NamaChallenge : String
    var TypeChallenge : String
    var BioChallenge : String
    var LearningGoalsChallenge : String
    var ConstrainsChallenge : String
    var DeliverablesChallenge : String
    var ImageBackground : String
    var DurationChallange : String
}
class ChallengeData {
    var ArrayChallenge : [Challenge] = [
        Challenge(ChallengeStatus : "On Going", NamaChallenge: "Challenge 2", TypeChallenge: "Individual Challenge", BioChallenge: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journey ahead. All explorers are expected to understand basic iOS development during this challenge.", LearningGoalsChallenge: "Applied Investigation\nEveryone can code", ConstrainsChallenge: "This is individual work\nNo third-party library\nUse HIG interfaces essentials", DeliverablesChallenge: "Sketch Prototype\nLearning Backlog\nDocument Xcode Project\nFeedback Report Document", ImageBackground: "challenge2", DurationChallange : "2 Week"),
        Challenge(ChallengeStatus : "Next Journey", NamaChallenge: "Challenge 3", TypeChallenge: "Group Challenge", BioChallenge: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journey ahead. All explorers are expected to understand basic iOS development during this challenge.", LearningGoalsChallenge: "Collaboration\nBest Practice\nCoding Understanding\nUser Best Practice\nProject Management", ConstrainsChallenge: "No constraint", DeliverablesChallenge: "SUser narration\nTrello for project\nWorking app with clean code", ImageBackground: "challenge3",DurationChallange : "4 Week"),
        Challenge(ChallengeStatus : "Next Journey", NamaChallenge: "Challenge 4", TypeChallenge: "Group Challenge", BioChallenge: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journey ahead. All explorers are expected to understand basic iOS development during this challenge.", LearningGoalsChallenge: "TBA", ConstrainsChallenge: "TBA", DeliverablesChallenge: "TBA", ImageBackground: "challenge4", DurationChallange : "4 Week"),
        Challenge(ChallengeStatus : "Next Journey", NamaChallenge: "Challenge 5", TypeChallenge: "Individual Challenge", BioChallenge: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journey ahead. All explorers are expected to understand basic iOS development during this challenge.", LearningGoalsChallenge: "TBA", ConstrainsChallenge: "TBA", DeliverablesChallenge: "TBA", ImageBackground: "challenge5", DurationChallange : "2 Week"),
        Challenge(ChallengeStatus : "Next Journey", NamaChallenge:  "Challenge 6", TypeChallenge: "Individual Challenge", BioChallenge: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journey ahead. All explorers are expected to understand basic iOS development during this challenge.", LearningGoalsChallenge: "TBA", ConstrainsChallenge: "TBA", DeliverablesChallenge: "TBA" ,ImageBackground: "challenge6", DurationChallange : "12 Week"),
          Challenge(ChallengeStatus : "Visited", NamaChallenge: "Challenge 1", TypeChallenge: "Group Challenge", BioChallenge: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journey ahead. All explorers are expected to understand basic iOS development during this challenge.", LearningGoalsChallenge: "Full Cycle\nCBL\nHIG (Human Interface Guidelines Prototyping Xcode & Basic SwiftGroup Collaboration", ConstrainsChallenge: "TBA", DeliverablesChallenge: "TBA", ImageBackground: "challenge1", DurationChallange : "5 Week"),
        ]
}
    
