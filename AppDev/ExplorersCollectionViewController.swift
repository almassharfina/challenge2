//
//  ExplorersCollectionViewController.swift
//  AppDev
//
//  Created by Almas Sharfina on 16/04/20.
//  Copyright © 2020 Almas Sharfina. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class ExplorersCollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == CollectionViewSenior{
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExplorerCell", for: indexPath) as! ExplorersCollectionViewCell
                   cell.explorersName.text = seniorManagerbebas[indexPath.item].expName
                   cell.explorersRole.text = seniorManagerbebas[indexPath.item].expRole
                   cell.explorersImage.image = UIImage(named: seniorManagerbebas[indexPath.item].expImage)
               return cell
               }
               else {
                   let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExpJunior", for: indexPath) as! ExplorersJuniorCollectionViewCell
                   cell.ExpJuniorName.text = juniorMorManagerbebas[indexPath.item].expName
                   cell.ExpJuniorRole.text = juniorMorManagerbebas[indexPath.item].expRole
                   cell.ExpJuniorImage.image = UIImage(named: juniorMorManagerbebas[indexPath.item].expImage)
                   return cell
                  }
    }
    
   
    var explorerManagerbebas  = ExplorerManager().arrayofExplorer
    var seniorManagerbebas :[Explorer] = []
    var juniorMorManagerbebas :[Explorer] = []
    
    @IBOutlet weak var CollectionViewSenior: UICollectionView!
    @IBOutlet weak var CollectionViewJunior: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        seniorManagerbebas = explorerManagerbebas.filter{
            $0.expLevel == "Senior"
        }
        juniorMorManagerbebas = explorerManagerbebas.filter{
            $0.expLevel == "Junior"
        }
        CollectionViewSenior.delegate = self
        CollectionViewSenior.dataSource = self
        CollectionViewJunior.delegate = self
        CollectionViewJunior.dataSource = self
        
        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource


func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
    if collectionView == CollectionViewSenior{
        return seniorManagerbebas.count
    }
    else {return juniorMorManagerbebas.count
    }

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == CollectionViewJunior{
            let vc = storyboard?.instantiateViewController(withIdentifier: "ExplorersDetail") as? ExplorersDetailViewController
            vc?.ExplorersDetail = juniorMorManagerbebas[indexPath.item]
           
            vc?.modalPresentationStyle = .fullScreen
                 self.present(vc!, animated: true, completion: nil)
        }
        else {
            let vc = storyboard?.instantiateViewController(withIdentifier: "ExplorersDetail") as? ExplorersDetailViewController
            vc?.ExplorersDetail = seniorManagerbebas[indexPath.item]
            
            vc?.modalPresentationStyle = .fullScreen
        self.present(vc!, animated: true, completion: nil)
        }
    }
}

//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExplorerCell", for: indexPath) as! ExplorersCollectionViewCell
       
        
    
        // Configure the cell
    
//        return cell


    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

