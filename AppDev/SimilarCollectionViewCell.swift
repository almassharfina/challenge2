//
//  SimilarCollectionViewCell.swift
//  AppDev
//
//  Created by Almas Sharfina on 16/04/20.
//  Copyright © 2020 Almas Sharfina. All rights reserved.
//

import UIKit

class SimilarCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var SimilarImage: UIImageView!
    @IBOutlet weak var SimilarRole: UILabel!
    @IBOutlet weak var SimilarName: UILabel!
}
