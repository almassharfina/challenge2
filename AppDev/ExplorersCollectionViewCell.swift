//
//  ExplorersCollectionViewCell.swift
//  AppDev
//
//  Created by Almas Sharfina on 16/04/20.
//  Copyright © 2020 Almas Sharfina. All rights reserved.
//

import UIKit

class ExplorersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var explorersImage: UIImageView!
    @IBOutlet weak var explorersName: UILabel!
    @IBOutlet weak var explorersRole: UILabel!
    
}
