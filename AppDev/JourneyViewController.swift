//
//  JourneyViewController.swift
//  AppDev
//
//  Created by Almas Sharfina on 13/04/20.
//  Copyright © 2020 Almas Sharfina. All rights reserved.
//

import UIKit

class JourneyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var ListJourney = ChallengeData()
    var ArrayJourney : [Challenge] = []

    var sectionArray : [Int:[Challenge]] = [0:[], 1:[], 2:[]]

    @IBOutlet weak var JourneyTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.JourneyTableView.tableHeaderView?.backgroundColor = UIColor.clear
        ArrayJourney = ListJourney.ArrayChallenge
        for Challenge in ArrayJourney{
            if Challenge.ChallengeStatus == "On Going"{
                sectionArray[0]?.append(Challenge)
            }
            else if Challenge.ChallengeStatus == "Next Journey"{
                sectionArray[1]?.append(Challenge)
            }
            else {
                sectionArray[2]?.append(Challenge)
            }
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionArray.count
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionArray[section]?[0].ChallengeStatus
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionArray[section]?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Cell = tableView.dequeueReusableCell(withIdentifier: "Challenge2") as! JourneyTableViewCell
        
        let ChallengeRow = self.sectionArray[indexPath.section]
        
        Cell.JourneyImageView.image = UIImage(named: ChallengeRow?[indexPath.item].ImageBackground ?? "")
        Cell.ChallengeLabel.text = ChallengeRow?[indexPath.item].NamaChallenge
        Cell.TypeChallengeLabel.text = ChallengeRow?[indexPath.item].TypeChallenge
    
        
        return Cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let StoryBoard = UIStoryboard (name: "Main", bundle: nil)
        let ChallengeRow = self.sectionArray[indexPath.section]
        
        let vc = StoryBoard.instantiateViewController(withIdentifier: "ChallengeViewController") as! ChallengeViewController
        vc.NamaChallenge = ChallengeRow! [indexPath.item].NamaChallenge
        vc.TipeChallenge = ChallengeRow![indexPath.item].TypeChallenge
        vc.FotoBackground = ChallengeRow![indexPath.item].ImageBackground
        vc.Constrains = ChallengeRow![indexPath.item].ConstrainsChallenge
        vc.Deliverables = ChallengeRow![indexPath.item].DeliverablesChallenge
        vc.WaktuChallenge = ChallengeRow![indexPath.item].DurationChallange
        vc.Learning = ChallengeRow![indexPath.item].LearningGoalsChallenge

        navigationController?.pushViewController(vc, animated: true)
    }
}

