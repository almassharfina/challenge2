//
//  ExplorerData.swift
//  AppDev
//
//  Created by Almas Sharfina on 16/04/20.
//  Copyright © 2020 Almas Sharfina. All rights reserved.
//

import Foundation

struct Explorer{
    let expName:String
    let expLevel:String
    let expShift:String
    let expRole:String
    let expImage:String
    let expCanHelp:String
    let expNeedHelp:String
}

class ExplorerManager{
    var arrayofExplorer: [Explorer]
    init() {
        self.arrayofExplorer = [
        Explorer(expName: "Ahmad Rizki Maulana",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Rizki_MO", expCanHelp: "", expNeedHelp: "Coding Basic"),
        Explorer(expName: "Albert Pangestu",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Albert_MO", expCanHelp: "Ideation.\n Speak English.\nUI and UX Design.", expNeedHelp: "Programming Basics."),
        Explorer(expName: "Alifudin Aziz",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Aziz_MO", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Amelia Verina Siswanto",expLevel: "Junior",expShift: "Morning",expRole: "Design", expImage: "Amelia_MO", expCanHelp: "Swift, UI/UX Design, Public Speaking, English Practice", expNeedHelp: "Design, Interior & Architecture Design."),
        Explorer(expName: "Angela Priscila Montolalu",expLevel: "Junior",expShift: "Morning",expRole: "Design", expImage: "Angel_MO", expCanHelp: "UI/UX Design, Swift, English Speaking", expNeedHelp: "Graphic Design (Branding)"),
        Explorer(expName: "Ardy Stephanus",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Ardy_MO", expCanHelp: "", expNeedHelp: "Basic Coding, Basic Swift"),
        Explorer(expName: "Arifin Firdaus",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Arifin_MO", expCanHelp: "", expNeedHelp: "iOS Development (UIKit), Clean Code"),
        Explorer(expName: "Arnold Pangestu",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Arnold_MO", expCanHelp: "", expNeedHelp: "Coding, Basic UI/UX Game Design, Unity;"),
        Explorer(expName: "Audhy Virabri Kressa",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Audhy_MO", expCanHelp: "", expNeedHelp: "- basic wift\n- git version controller\n- parsing data"),
        Explorer(expName: "Baby Amelia Andina Putri",expLevel: "Junior",expShift: "Morning",expRole: "Other", expImage: "Baby_MO", expCanHelp: "- Swift \n- xCode\n- UI/UX design", expNeedHelp: "Business insights, public speaking and presentation skill, basic design."),
        Explorer(expName: "Bagus Setiawan",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Bagus_MO", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Benyamin Rondang Tuahta",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Ben_MO", expCanHelp: "Leading & trailing swipe action", expNeedHelp: "Basic coding"),
        Explorer(expName: "Dinie P Hemas",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Dinie_MO", expCanHelp: "Public speaking, English", expNeedHelp: "Basic coding, UI pattern"),
        Explorer(expName: "Dionesia Nadya Dewayani",expLevel: "Junior",expShift: "Morning",expRole: "Design", expImage: "Nadya_MO", expCanHelp: "", expNeedHelp: "Illustration"),
        Explorer(expName: "Edward da Costa",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Edward_MO", expCanHelp: "Design , Advance Code", expNeedHelp: "Basic Code"),
        Explorer(expName: "Faris Rasyadi Putra",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Faris_MO", expCanHelp: "", expNeedHelp: "Unity 3D Programmer"),
        Explorer(expName: "Ferdinan Linardi",expLevel: "Junior",expShift: "Morning",expRole: "Design", expImage: "Ferdinan_MO", expCanHelp: "Swift, UI/UX Design, Copywriting, Public Speaking, English Practice", expNeedHelp: "Ilustration, Graphic Design, Fashion Styling"),
        Explorer(expName: "Geraldine Janice Wibisono",expLevel: "Junior",expShift: "Morning",expRole: "Design", expImage: "Janice_MO", expCanHelp: "Intermediate coding, translating design into code", expNeedHelp: "Graphic design, Illustration"),
        Explorer(expName: "Hana Christina Rondoh",expLevel: "Junior",expShift: "Morning",expRole: "Design", expImage: "Hana_MO", expCanHelp: "XCode, Swift, UI/UX Design, English Practice, English Public Speaking, English copywriting", expNeedHelp: "Fashion design, Graphic design, Fashion business and styling"),
        Explorer(expName: "Henry Christanto Sutjiono",expLevel: "Junior",expShift: "Morning",expRole: "Design", expImage: "Henry_MO", expCanHelp: "Basic Coding for XCode, designing prototype with Sketch", expNeedHelp: "Photography, Copywriting & Advertising"),
        Explorer(expName: "Ibrahim Yunus Muhammad Fiqhan",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Fiqhan_MO", expCanHelp: "English, Graphic Design, Custom Component in Swift (Star Rating)", expNeedHelp: "Basic programing, Object Oriented Programing (OOP), Software Design Pattern"),
        Explorer(expName: "James Tirto",expLevel: "Junior",expShift: "Morning",expRole: "Other", expImage: "James_MO", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Janice Budihartono",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "J'Ice_MO", expCanHelp: "Auto layout, Intermediate Swift Code (maybe), Design", expNeedHelp: "- Code\n- English"),
        Explorer(expName: "Jeffrey Phinardi Kosasih",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Jep_MO", expCanHelp: "Public Speaking, Xcode (Constraint, Chat Feature, and Verification Code)", expNeedHelp: "Youtube content, Pr, Ph, Ai, Basic coding,"),
        Explorer(expName: "Jeffry Sandy Purnomo",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Jeffry_MO", expCanHelp: "Swift UI", expNeedHelp: "Basic Code, Basic Sketch"),
        Explorer(expName: "Jesslyn Faustina",expLevel: "Junior",expShift: "Morning",expRole: "Design", expImage: "Jesslyn_MO", expCanHelp: "Intermediate coding", expNeedHelp: "Graphic Design"),
        Explorer(expName: "Josephine Sugiharto",expLevel: "Junior",expShift: "Morning",expRole: "Design", expImage: "Jose_MO", expCanHelp: "Basic Coding", expNeedHelp: "Branding, Graphic Design"),
        Explorer(expName: "Karina Enny Agustina",expLevel: "Junior",expShift: "Morning",expRole: "Other", expImage: "Karina_MO", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Leonora Jeanifer",expLevel: "Junior",expShift: "Morning",expRole: "Other", expImage: "Jean_MO", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Lois Pangestu",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Lois_MO", expCanHelp: "", expNeedHelp: "Basic Coding, Basic Swift, Idea"),
        Explorer(expName: "M. Rizky Hidayat",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Rizky_MO", expCanHelp: "- English speak\n- Swift code style\n- Self management", expNeedHelp: "Basic Code, SysAdmin"),
        Explorer(expName: "Martin Ivo Hardinoto",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Martin_MO", expCanHelp: "", expNeedHelp: "- Basic swift\n\n\n- Ask me everything about covid-19"),
        Explorer(expName: "Michael Randicha Gunawan Santoso",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Michael_MO", expCanHelp: "Someone with Intermediate / Advanced understanding on TableViewController, TableViewCell, InputAccessoryView, TableViewCell's item's constraint & Protocol", expNeedHelp: "Coder : Basic Swift, Intermediate Javascript (Angular & Ionic), Intermediate Laravel, Interested in Flutter"),
        Explorer(expName: "Moh. Zinnur Atthufail Addausi",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "A'il_MO", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Najibullah Ulul Albab",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Ulul_MO", expCanHelp: "Swift Intermediate, UX Designer", expNeedHelp: "Code, UI/UX Pattern"),
        Explorer(expName: "Natanael Geraldo Santoso",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Natanael_MO", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Nicholas Ang",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Nicholas_MO", expCanHelp: "", expNeedHelp: "Videography, Photography, Game Development (Unity), Animation and VFX, Basic Code"),
        Explorer(expName: "Nur Afni Zuhrotul Laili",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Afni_MO", expCanHelp: "", expNeedHelp: "- basic swift\n- passing data between viewcontroller\n- set onboarding screen"),
        Explorer(expName: "Oktavia Citra Resmi Rachmawati",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Citra_MO", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Rachel Audrey Effendi",expLevel: "Junior",expShift: "Morning",expRole: "Design", expImage: "Rachel_MO", expCanHelp: "Basic coding", expNeedHelp: "Graphic Design, Branding"),
        Explorer(expName: "Rizal Hidayat",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Rizal_MO", expCanHelp: "English", expNeedHelp: "Basic code / basic swift"),
        Explorer(expName: "Siti Istiany Tribunda",expLevel: "Junior",expShift: "Morning",expRole: "Design", expImage: "Isti_MO", expCanHelp: "-English speaking\n-Public speaking\n-Team work\n- Coding, Swift, UI&UX Design.", expNeedHelp: "Graphic Design :\nBranding\nLayout\n\n\nSocial Media Enthusiast :p"),
        Explorer(expName: "Steven Wijaya",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "SW_MO", expCanHelp: "", expNeedHelp: "Basic Code, Basic Swift, iOS dev, a bit MacOS dev, Laravel, Backend, etc."),
        Explorer(expName: "Taufiq Ramadhany",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Taufiq_MO", expCanHelp: "", expNeedHelp: "Basic swift"),
        Explorer(expName: "Tony Varian Yoditanto",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Tony_MO", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Wahyu Kharisma Mujiono",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Wahyujus_MO", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Wahyu Saputra",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Wahyu_MO", expCanHelp: "Photoeditor, English Speaking", expNeedHelp: "Basic Coding"),
        Explorer(expName: "Winata Arafat Fal Aham",expLevel: "Junior",expShift: "Morning",expRole: "Tech", expImage: "Badai_MO", expCanHelp: "UI/UX Design, Swift, English Speaking", expNeedHelp: "Photography, Basic Coding, Critical Thinking, git,"),
        Explorer(expName: "Yonatan Niko Sucahyo",expLevel: "Junior",expShift: "Morning",expRole: "Other", expImage: "Niko_MO", expCanHelp: "basic coding, graphic Design", expNeedHelp: "Business Management"),
        Explorer(expName: "Ari Kurniawan",expLevel: "Senior",expShift: "All",expRole: "Design Facilitator", expImage: "Ari_SE", expCanHelp: "Basic coding", expNeedHelp: "Graphic Design (layout/composition, typography, branding), HIG, UI/UX"),
        Explorer(expName: "Bayu Prasetya",expLevel: "Senior",expShift: "All",expRole: "Design Facilitator", expImage: "Bayu_SE", expCanHelp: "Visual Branding, Design Thinking, Development Process, UI/UX, Nemenin Mikir, Makan-makan (soon)", expNeedHelp: ""),
        Explorer(expName: "Dickson Leonard",expLevel: "Senior",expShift: "All",expRole: "Academy Manager", expImage: "Dickson_SE", expCanHelp: "", expNeedHelp: "Coding basics (algorithm, data structure, operations)\nApp development process (problem definition, ideation, prototyping, production), Basic UX, Professional skills (leadership, communication)"),
        Explorer(expName: "Fanny Halim",expLevel: "Senior",expShift: "All",expRole: "Coding Facilitator", expImage: "Fanny_SE", expCanHelp: #"English Speaking \^0^/"#, expNeedHelp: "Basic iOS Development\n(Swift, XCode, Auto Layout, Data Collection)\nDiscussion"),
        Explorer(expName: "Gabriele Wijasa",expLevel: "Senior",expShift: "All",expRole: "Design Facilitator", expImage: "Gabriele_SE", expCanHelp: "", expNeedHelp: "Graphic Design, UI/UX, Photography"),
        Explorer(expName: "Januar Tanzil",expLevel: "Senior",expShift: "All",expRole: "Coding Facilitator", expImage: "Januar_SE", expCanHelp: "", expNeedHelp: "Swift language, IOS Development, Game Development, Software Engineering in general"),
        Explorer(expName: "Jaya Pranata",expLevel: "Senior",expShift: "All",expRole: "Coding Facilitator", expImage: "Jaya_SE", expCanHelp: "Graphic Design (layout/composition, typography, branding), UI/UX, problem analysis, basic research, HIG", expNeedHelp: "Coding basic, Swift Language, Auto Layout, Core Data, Map"),
        Explorer(expName: "John Alan Ketaren",expLevel: "Senior",expShift: "All",expRole: "Professional Facilitator", expImage: "Ketaren_SE", expCanHelp: "Basic Xcode", expNeedHelp: "Ideation, bringing resources, user testing, business, fintech, Presentation,Networking."),
        Explorer(expName: "Rachmat Kukuh Rahadiansyah",expLevel: "Senior",expShift: "All",expRole: "Coding Facilitator", expImage: "Kukuh_SE", expCanHelp: "", expNeedHelp: "- General iOS app development\n- iOS frameworks\n- Swift language"),
        Explorer(expName: "Yehezkiel Cheryan Tjandra",expLevel: "Senior",expShift: "All",expRole: "Professional Facilitator", expImage: "Ryan_SE", expCanHelp: "Swift/Xcode in general", expNeedHelp: "English language, Public speaking, Business presentation, Business writing, Business insights"),
        Explorer(expName: "Yulibar Husni",expLevel: "Senior",expShift: "All",expRole: "Coding Facilitator", expImage: "Yulibar_SE", expCanHelp: "- Ideation\n- English Speaking\n- Project Management", expNeedHelp: "- Swift \n- Ideation\n- Animation production"),
        Explorer(expName: "Abraham Christopher",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Abraham_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Afitra Mamor Bikhoir",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Afitra_AF", expCanHelp: "how to implement crud in swift", expNeedHelp: "Basic Coding"),
        Explorer(expName: "Alessandro Vieri",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Aldo_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Almas Sharfina Puspandam",expLevel: "Junior",expShift: "Afternoon",expRole: "Design", expImage: "Almas_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Andre Marines Ado Tena Uak",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Matu_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Angelica Irene Christina",expLevel: "Junior",expShift: "Afternoon",expRole: "Other", expImage: "Ren_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Angelina Chandra",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Angel_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Arinal Haq",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Arinal_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Auriga Aristo",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Riga_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Candra Sabdana Nugroho",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Candra_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Christian Tanjono",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Christian_AF", expCanHelp: "- Advance OOP\n- UI Design\n- Transition on Swift (Onboarding)", expNeedHelp: "- Basic Coding\n- Basic Programming Algorithm\n- Network Troubleshooting\n- Achieving your Body Goals"),
        Explorer(expName: "Christopher Kevin Susandji",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Kikis_AF", expCanHelp: "SQLite CRUD :(((", expNeedHelp: "Basic Swift, Basic Coding, Basic UI and Graphic Design, software-related stuffs (e.g. installing Minecraft, uninstalling Fortnite)"),
        Explorer(expName: "David Christian Kartamihardja",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "David_AF", expCanHelp: "HIG, Layouting, design, UI/UX, English presentation", expNeedHelp: "Basic swift, basic coding"),
        Explorer(expName: "Dimarta Julkha Faradiba",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Marta_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Fauzia Umar",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Zizi_AF", expCanHelp: "", expNeedHelp: "basic coding, basic swift, UIUX"),
        Explorer(expName: "Felicia Gunadi",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Felicia_AF", expCanHelp: "Code, Presentation", expNeedHelp: "Basic Coding, UI Design, Team Management"),
        Explorer(expName: "Harvey Lienardo",expLevel: "Junior",expShift: "Afternoon",expRole: "Design", expImage: "Harvey_AF", expCanHelp: "", expNeedHelp: "Photography, image processing, layout"),
        Explorer(expName: "I Putu Andhika Indrastata Widyadhana",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Putu_AF", expCanHelp: "Take image data from taken photos", expNeedHelp: "-Basic Coding, Basic Sketch\n-Having Fun"),
        Explorer(expName: "Ivan winarto",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Ivan_AF", expCanHelp: "present properly in english", expNeedHelp: "Basic Coding\nTeam management"),
        Explorer(expName: "Jeremy Endratno",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Jeremy_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Jeslyn Kosasih",expLevel: "Junior",expShift: "Afternoon",expRole: "Design", expImage: "Jeslyn_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Jessica Ayumi Aris",expLevel: "Junior",expShift: "Afternoon",expRole: "Design", expImage: "Ayumi_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Jessy The Wirianto",expLevel: "Junior",expShift: "Afternoon",expRole: "Other", expImage: "Jessy_AF", expCanHelp: "Coding\nDesign\nCommunication Skill", expNeedHelp: "Business Idea\nDesign Input"),
        Explorer(expName: "Joahan Wirasugianto",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Joahan_AF", expCanHelp: "- Code\n- UI Design", expNeedHelp: "Code, UI Design, Get Reletionship, Achieving your Body Goals"),
        Explorer(expName: "Jovan Alvin",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Jovan_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Kevin Heryanto",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "KH_AF", expCanHelp: "Code, Design, HIG, English Speaking", expNeedHelp: "Code, Makan-Makan."),
        Explorer(expName: "Kevin Pratama Soedarso",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Kp_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Kezia Lauren Handoyo",expLevel: "Junior",expShift: "Afternoon",expRole: "Design", expImage: "Kezia_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Krisna Harimurti",expLevel: "Junior",expShift: "Afternoon",expRole: "Other", expImage: "Krisna_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Livia Angelica Gunawan",expLevel: "Junior",expShift: "Afternoon",expRole: "Design", expImage: "Livia_AF", expCanHelp: "", expNeedHelp: "Basic graphic design stuff (Photoshop, Illustrator), English maybe hehe, copywriting"),
        Explorer(expName: "Michelle Caroline Kristanto",expLevel: "Junior",expShift: "Afternoon",expRole: "Design", expImage: "Michelle_AF", expCanHelp: "", expNeedHelp: "Graphic Design (Layouting)"),
        Explorer(expName: "Moch Maulana Ardiansyah",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Maul_AF", expCanHelp: "", expNeedHelp: "Basic Swift, UI/UX Design, Graphic Design, Layouting, Coding, Logical Thinking"),
        Explorer(expName: "Monica Adelya Putri",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Monica_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Muhammad Bangun Agung",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Agung_AF", expCanHelp: "Presentation, Collaboration", expNeedHelp: "Coding, UI Design, Animation"),
        Explorer(expName: "Muhammad Faisal Febrianto",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Faisal_AF", expCanHelp: "ui,ux and english speaking", expNeedHelp: "basic coding"),
        Explorer(expName: "Muhammad Naufal Muzaky Sukmana Putra",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Zaky_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Natasha Adeline Wardhana",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Tata_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Richard Santoso",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Richard_AF", expCanHelp: "", expNeedHelp: "basic coding, English"),
        Explorer(expName: "Ricky Austin Setiabudi",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Ricky_AF", expCanHelp: "", expNeedHelp: "Layouting, Logical Thinking, Basic Swift, photo manipulation, and pushing your score in the game"),
        Explorer(expName: "Rini Handini",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Rini_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Ryan Anslyno Khohari",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Ryan_AF", expCanHelp: "Code with Model View", expNeedHelp: "Basic Coding"),
        Explorer(expName: "Samantha Teonata",expLevel: "Junior",expShift: "Afternoon",expRole: "Design", expImage: "Sam_AF", expCanHelp: "", expNeedHelp: "Graphic Design, bonding and getting along with people, ngeramein acara (BUKAN badut ultah), boosting your confidence (if you feel like you need one), ngobrol dan ngoceh"),
        Explorer(expName: "Silviera Dewi Nagari",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Silviera_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Stefany",expLevel: "Junior",expShift: "Afternoon",expRole: "Other", expImage: "Stefany_AF", expCanHelp: "pengen sih bisa gambar wkwk", expNeedHelp: ""),
        Explorer(expName: "Timotius Cahyadi",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Timotius_AF", expCanHelp: "", expNeedHelp: "Basic Coding, Basic Design"),
        Explorer(expName: "Timotius Leonardo Lianoto",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Timol_AF", expCanHelp: "Design and how to pick a good colour collaboration", expNeedHelp: "Code Logic, Public Speaking"),
        Explorer(expName: "Tivany Hartono",expLevel: "Junior",expShift: "Afternoon",expRole: "Design", expImage: "Tivany_AF", expCanHelp: "", expNeedHelp: "Graphic Design"),
        Explorer(expName: "Ulinnuha Nabilah",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Nabilah_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "Victor Wijaya Ngantung",expLevel: "Junior",expShift: "Afternoon",expRole: "Other", expImage: "Victor_AF", expCanHelp: "Coding", expNeedHelp: "Business\nDesign Basics"),
        Explorer(expName: "Wienona Martha Parlina",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "Wienona_AF", expCanHelp: "", expNeedHelp: ""),
        Explorer(expName: "William Sebastian Thedja",expLevel: "Junior",expShift: "Afternoon",expRole: "Tech", expImage: "William_AF", expCanHelp: "", expNeedHelp: "")
        ]
        
        
        }
    
    
    
}
