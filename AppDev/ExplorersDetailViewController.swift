//
//  ExplorersDetailViewController.swift
//  AppDev
//
//  Created by Almas Sharfina on 16/04/20.
//  Copyright © 2020 Almas Sharfina. All rights reserved.
//

import UIKit

class ExplorersDetailViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var ExplorersDetail: Explorer?
    var ArrayOfExplorer = ExplorerManager().arrayofExplorer
    
    var ArrayOfSimilar: [Explorer] = []
    
    
    @IBOutlet weak var ProfileImage: UIImageView!
    @IBOutlet weak var ExpName: UILabel!
    @IBOutlet weak var DetailWantLearn: UILabel!
    @IBOutlet weak var DetailCanHelp: UILabel!
    @IBOutlet weak var ExpRole: UILabel!
    @IBOutlet weak var CollectionSimilar: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ProfileImage.image = UIImage(named: ExplorersDetail?.expImage ?? "")
        ExpName.text = ExplorersDetail?.expName
        DetailCanHelp.text = ExplorersDetail?.expCanHelp
        DetailWantLearn.text = ExplorersDetail?.expNeedHelp
        ExpRole.text = ExplorersDetail?.expRole
        
        ArrayOfSimilar = ArrayOfExplorer.filter{
            $0.expName != ExplorersDetail?.expName &&  $0.expRole == ExplorersDetail?.expRole
    
        }
        CollectionSimilar.delegate = self
        CollectionSimilar.dataSource = self
    }
    
    @IBAction func ExplorersButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ArrayOfSimilar.count
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = CollectionSimilar.dequeueReusableCell(withReuseIdentifier: "SimilarCollectionViewCell", for: indexPath) as! SimilarCollectionViewCell
        cell.SimilarImage.image = UIImage(named: ArrayOfSimilar[indexPath.item].expImage)
        cell.SimilarName.text = ArrayOfSimilar[indexPath.item].expName
        cell.SimilarRole.text = ArrayOfSimilar[indexPath.item].expRole
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let parentVC = presentingViewController
        self.dismiss(animated: false, completion: {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ExplorersDetail") as? ExplorersDetailViewController
            vc?.ExplorersDetail = self.ArrayOfSimilar[indexPath.item]
            parentVC!.present(vc!,animated: true)
        })
    }
        /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
